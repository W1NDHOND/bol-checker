import axios from 'axios';

axios.defaults.baseURL = process.env.fbDBUrl;

export const ProductsApi = {
    getProducts(token) {
        return axios.get(`/products.json?auth=${token}`);
    },
    editProduct(product) {
        return axios.put('/products/' + product.id + '.json', product)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
    },
    updateProduct(productId, updatedProduct, token) {
        return axios.patch(`/products/${productId}.json?auth=${token}`, updatedProduct);
    },
    addProduct(product, token) {
        return axios.post(`/products.json?auth=${token}`, product)
            .then((response) => {
                product.id = response.data.name;
                return axios.put(`/products/${product.id}.json?auth=${token}`, product)
                    .then(response => {
                        console.log(response);
                        return true;
                    })
                    .catch(error => {
                        console.log(error);
                        return false;
                    });

            });
    },
    removeProduct(product, token) {
        return axios.delete(`/products/${product.id}.json?auth=${token}`);
    }
};
