import Vuex from 'vuex';

import UserStore from './user/UserStore';

const store = () => {
    return new Vuex.Store({
        modules: {
            UserStore
        }
    });
};

export default store;
