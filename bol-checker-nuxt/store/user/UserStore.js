import Cookie from 'js-cookie';

const defaultState = () => {
    return {
        token: null,
        user: null
    }
};

export default {
    namespaced: true,

    state: defaultState(),

    mutations: {
        setToken(state, token) {
            state.token = token;
        },
        clearToken(state) {
            state.token = null;
        },
        setUser(state, user) {
            state.user = user;
        },
        clearUser(state) {
            state.user = null;
        },
        reset(state) {
            Object.assign(state, defaultState());
        }
    },

    actions: {
        authenticateUser(context, authData) {
            return this.$axios.$post('https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=' +
                process.env.fbAPIKey,
                {
                    email: authData.username,
                    password: authData.password,
                    returnSecureToken: true
                })
                .then(result => {
                    context.commit('setUser', result.email);
                    context.commit('setToken', result.idToken);
                    if (process.client) {
                        localStorage.setItem('token', result.idToken);
                        localStorage.setItem('tokenExpiration', new Date().getTime() + +result.expiresIn * 1000);
                    }
                    Cookie.set('jwt', result.idToken);
                    Cookie.set('expirationDate', new Date().getTime() + +result.expiresIn * 1000);
                })
                .catch(error => {
                    console.error('onSubmit error: ', error);
                });
        },
        initAuth(context, req) {
            let token;
            let expirationDate;
            if (req) {
                if (!req.headers.cookie) {
                    return;
                }
                const jwtCookie = req.headers.cookie
                    .split(';')
                    .find(c => c.trim().startsWith('jwt='));
                if (!jwtCookie) {
                    return;
                }
                token = jwtCookie.split('=')[1];
                expirationDate = req.headers.cookie
                    .split(';')
                    .find(c => c.trim().startsWith('expirationDate='))
                    .split('=')[1];
            } else {
                if (process.client) {
                    token = localStorage.getItem('token');
                    expirationDate = localStorage.getItem('tokenExpiration');
                }
            }
            if (new Date().getTime() > +expirationDate || !token) {
                console.log('No token or invalid token');
                context.dispatch('logout');
                return;
            }
            context.commit('setToken', token);
        },
        logout(context) {
            context.commit('clearUser');
            context.commit('clearToken');
            Cookie.remove('jwt');
            Cookie.remove('expirationDate');
            if (process.client) {
                localStorage.removeItem('token');
                localStorage.removeItem('tokenExpiration');
            }
        },
        reset(context) {
            context.commit('reset');
        }
    },

    getters: {
        isAuthenticated(state) {
            return state.token !== null;
        },
        getUserIdToken(state) {
            return state.token;
        },
        getUser(state) {
            return state.user;
        }
    }
};
