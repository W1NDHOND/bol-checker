export default function (context) {
    if (!context.store.getters['UserStore/isAuthenticated']) {
        context.redirect('/login');
    }
}
