import pkg from './package';

module.exports = {
    mode: 'universal',

    /*
    ** Headers of the page
    */
    head: {
        title: pkg.name,
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: pkg.description}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:300,400,700'},
            {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Rubik:300,400,500,700'},
            {rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons'}
        ]
    },

    /*
    ** Customize the progress-bar color
    */
    loading: {color: '#FFFFFF'},

    /*
    ** Global CSS
    */
    css: [
        '~/assets/css/reset.scss',
        '~/assets/css/base.scss',
        '~/assets/css/transitions.scss'
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        {src: '~/plugins/localStorage.js', ssr: false}
    ],

    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://github.com/nuxt-community/axios-module#usage
        '@nuxtjs/axios',
        'nuxt-sass-resources-loader'
    ],

    /*
    ** Sass Resources imports
    */
    sassResources: [
        '@/assets/css/grid.scss',
        '@/assets/css/colors.scss'
    ],
    /*
    ** Axios module configuration
    */
    axios: {
        // See https://github.com/nuxt-community/axios-module#options
    },

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
            // Run ESLint on save
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        },
        postcss: [
            require('autoprefixer')({
                browsers: ['last 4 versions']
            })
        ]
    },
    env: {
        // // Settings Profiles Test
        // fbAPIKey: 'AIzaSyAXQIbo7rxlaVB9sxuSxVPkXZSULevBqJA',
        // fbDBUrl: 'https://bol-koopblok-checker.firebaseio.com',
        // // bolApiUrl: 'http://bolchecker.somee.com',
        // bolApiUrl: 'http://localhost:34563',
        // company: 'MRcommerce',
        // competitor: 'SEWebsales',
        // localStorageKey: 'bol-checker-test'

        // // Settings Profiles Rutger
        // fbAPIKey: 'AIzaSyCYZl7rCXahu-xnZDkNEks7HnaM82VWUvo',
        // fbDBUrl: 'https://bol-rutger.firebaseio.com',
        // bolApiUrl: 'http://bolchecker.somee.com',
        // company: 'MRcommerce',
        // competitor: 'SEWebsales',
        // localStorageKey: 'bol-checker-rutger'

        // Settings Profiles Ewoud
        fbAPIKey: 'AIzaSyAf81e7WKRzGJg6EgpUvGZ07DS-EyF5pFs',
        fbDBUrl: 'https://bol-ewoud.firebaseio.com',
        bolApiUrl: 'http://bolchecker.somee.com',
        company: 'SEWebsales',
        competitor: 'MRcommerce',
        localStorageKey: 'bol-checker-ewoud'
    },
    router: {
        // Base directory for production
        base: '/ewoud/'
    }
}
