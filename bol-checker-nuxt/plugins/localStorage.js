import createPersistedState from 'vuex-persistedstate';

export default ({store, isHMR}) => {
    if (isHMR) {
        return;
    } else {
        window.onNuxtReady((nuxt) => {
            createPersistedState({
                key: process.env.localStorageKey
            })(store)
        })
    }
}
