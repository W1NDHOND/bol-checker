//Common
export {default as Loader} from './common/Loader.vue';
export {default as Popup} from './common/Popup.vue';

//Products
export {default as AddProducts} from './Products/AddProducts';
export {default as EditProduct} from './Products/EditProduct';
