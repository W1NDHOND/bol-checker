﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace BolApi.Controllers
{
	[RoutePrefix("api")]
	public class BolApiController : ApiController
	{
		[HttpGet]
		[Route("checksellingblock")]
		public IHttpActionResult CheckSellingBlock(string url, string company, string competitor)
		{
			// Create request to the given URL
			WebClient client = new WebClient();
			string readerResponse = client.DownloadString(url).Replace(" ", "");

			string sellingBlockString = "<spanclass=\"sbsb-badge-info-neg\">";
			string sellingByBolString = "<divclass=\"buy-block__seller-name\"data-test=\"seller-name\">\n\nVerkoopdoor\nbol.com\n\n</div>";
			string scarcityString = "eVar27=notscarcity";

			// Read the URL for parameters
			Boolean sellingBlock = readerResponse.Contains(company + sellingBlockString);
			Boolean sellingByCompetitor = readerResponse.Contains(competitor + sellingBlockString);
			Boolean sellingByBol = readerResponse.Contains(sellingByBolString);
			Boolean productScarcity = !readerResponse.Contains(scarcityString);
			Boolean onSummary = false;

			if (!sellingBlock) {
				string readerResponseSummary = client.DownloadString(url + "/prijsoverzicht/").Replace(" ", "");
				double remainingPages = Math.Floor(double.Parse(readerResponseSummary.GetBetween("\"newCount\":", ",")) / 10);
				onSummary = readerResponseSummary.Contains(company);

				if (remainingPages > 0 && !onSummary)
				{
					for (int i = 0; i < remainingPages; i++)
					{
						int currentPage = i + 2;
						string readerTemp = client.DownloadString(url + "/prijsoverzicht/?page=" + currentPage).Replace(" ", "");
						onSummary = readerTemp.Contains(company);
						if (onSummary) {
							break;
						}
					}
				}
			}

			// Create the data for the response object
			var data = new
			{
				sellingBlock,
				sellingByCompetitor,
				sellingByBol,
				productScarcity,
				onSummary,
			};

			// Testing purposes
			//var dataTemp = new
			//{
			//	readerResponse
			//};

			// Create the response object
			var results = new
			{
				url,
				result = data
			};

			// Return the results
			return Ok(results);
		}

		[HttpGet]
		[Route("getresponse")]
		public IHttpActionResult GetResponse(string url)
		{
			// Create request to the given URL
			WebClient client = new WebClient();
			string readerResponse = client.DownloadString(url).Replace(" ", "");

			// Create the response object
			var results = new
			{
				url,
				result = readerResponse
			};
			
			// Return the results
			return Ok(results);
		}

		[HttpGet]
		[Route("productinfo")]
		public IHttpActionResult ProductInfo(string url)
		{
			// Create request to the given URL
			WebClient client = new WebClient();
			string readerResponse = client.DownloadString(url);

			//string text = "This is an example string and my data is here";
			string productName = readerResponse.GetBetween("<title>bol.com | ", "</title>");
			string productEan = readerResponse.GetBetween("data-ean=\"", "\"");

			// Create the data for the response object
			var data = new
			{
				productName,
				productEan
			};

			// Create the response object
			var results = new
			{
				url,
				result = data
			};

			// Return the results
			return Ok(results);
		}
	}
}
